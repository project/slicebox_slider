# Slice Block Slider

Description
-----------
Drupal module to create a simple custom block 3D slider.

Based on the JS library slicebox to manage the behavior of the slider. 

The slider is made using the paragraph module for Drupal 8.

The HTML output of the slider can be completely overwritten by your theme via twig templates.
 
After the module is installed you're free to modify the structure of the paragraph by adding or removing new fields. 


For more information about slicebox.js visit the page: https://tympanus.net/Development/Slicebox/

Requirements
------------
Drupal 8.x

Paragraph


Support
-------
skcptsuresh359@gmail.com
